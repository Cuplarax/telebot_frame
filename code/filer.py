from code.color import Color

PARAMS_PATH = __file__.removesuffix('filer.py') + "params\\"


class Filer(object):
    @staticmethod
    def get(name: str, is_digit: bool = True):
        try:
            with open(PARAMS_PATH + name, 'r') as file:
                lines = file.readlines()
                if len(lines) == 0:
                    raise RuntimeError(f'file {name} is empty')
                else:
                    done = lines[0]
        except FileNotFoundError:
            raise FileNotFoundError(f'file {name} is not exist in params directory')
        done = done.strip()
        if is_digit:
            done = int(done)
        return done

    @staticmethod
    def create(filename: str, line: str):
        with open(PARAMS_PATH + filename, 'w+') as file:
            file.write(line)

    @staticmethod
    def create_get(filename: str, message: str, is_digit: bool, is_forced: bool):
        error_occurred = False  # did an error occurred while receiving the file (not found)
        try:
            with open(PARAMS_PATH + filename, "r"):
                pass
        except FileNotFoundError:
            error_occurred = True
        if error_occurred or is_forced:  # if error occurred or forced loading enabled
            Color.cprint('cyan', message.center(29, '='), False)
            if is_digit:
                Color.cprint('cyan', ' (digit)')
            else:
                print()
            Color.cprint('blue', '> ', False)
            line = input()
            Filer.create(filename, line)
        return Filer.get(filename, is_digit)
