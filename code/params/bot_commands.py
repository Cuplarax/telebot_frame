from code.params import config
import random

command_list_dict = {
    'help': 'send list of all commands',
    'start': 'send start message',
    'random': 'send random number(float)'
}

command_list_mes = ''

for c in command_list_dict.keys():
    command_list_mes += f'/{c} - {command_list_dict[c]}\n'


@config.BOT.message_handler(commands=['start', 'launch', 'run'])
def handle_text(message):
    config.BOT.send_message(message.chat.id, 'welcome\nsend \"/help\" or \"/?\" for list of commands')


@config.BOT.message_handler(commands=['?', 'help'])
def help_message(message):
    config.BOT.send_message(message.chat.id, command_list_mes)


@config.BOT.message_handler(commands=['rand', 'random'])
def random_number(message):
    config.BOT.send_message(message.chat.id, f'{random.random()}')
