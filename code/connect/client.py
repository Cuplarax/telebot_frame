import socket
import sys

PORT = int(sys.argv[1])
PKG_SIZE = int(sys.argv[2])

success = False
sock = socket.socket()

while not success:
    try:
        sock.connect(('localhost', PORT))
    except ConnectionRefusedError:
        print('fail.')
        input('press ENTER to try again')
    else:
        success = True
print('===[connected]===')
while True:
    try:
        sock.send(str.encode(input('> ')))
        data = sock.recv(PKG_SIZE).decode()
        if data == 'stop':
            break
    except Exception:
        print('===[disconnect]===')
        break
    print(data)
sock.close()
input('press ENTER to exit\n')
