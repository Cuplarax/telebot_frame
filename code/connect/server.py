import socket
from code.params import config
from code import color
import subprocess
from sys import executable


class Server(object):
    _sock = socket.socket()
    _run = True
    _conn = None

    @staticmethod
    def send(message: str):
        Server._conn.send(str.encode(message))

    @staticmethod
    def start_server():
        try:
            Server._sock.bind(('', config.PORT))
            Server._sock.listen(1)
            color.Color.cprint('green', 'local server is running...')
            color.Color.cprint('orange', 'connection is excepted')
            subprocess.Popen([executable, __file__.removesuffix('server.py') + 'client.py',
                              str(config.PORT), str(config.PKG_SIZE)],
                             creationflags=subprocess.CREATE_NEW_CONSOLE)
            while Server._run:
                Server._conn, address = Server._sock.accept()
                color.Color.cprint('green', f'connected: {address[0]} | {address[1]}')
                while True:
                    if Server._conn.fileno() == -1:
                        break
                    data = Server._conn.recv(config.PKG_SIZE)
                    if not data:
                        break
                    match data.decode().lower():
                        case 'stop':
                            Server.send('stop')
                            Server._conn.close()
                            color.Color.cprint('red', 'local server was stopped')
                            Server._run = False
                            config.BOT.stop_bot()
                            color.Color.cprint('red', 'bot stops...')
                        case 'hello':
                            Server.send('hi!')
                        case 'params':
                            Server.send(f'port: {config.PORT}\npackage size: {config.PKG_SIZE} bytes')
                        case _:
                            Server.send('unknown command')
        finally:
            Server._conn.close()
