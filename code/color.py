from sys import platform, stdout
from code.params import config
import subprocess
from os import system

LOGO = """
   _____            _                      
  / ____|          | |                     
 | |    _   _ _ __ | | __ _ _ __ __ ___  ___
 | |   | | | | '_ \\| |/ _` | '__/ _` \\ \\/ /
 | |___| |_| | |_) | | (_| | | | (_| |>  <| 
  \\_____\\__!_| .__/|_|\\__,_|_|  \\__,_/_/\\_\\ 
             | |                           
             |_|                           
"""

colors = {
    'white': '\033[0;0m',
    'red': '\033[0;31m',
    'green': '\033[0;32m',
    'orange': '\033[0;33m',
    'blue': '\033[0;34m',
    'purple': '\033[0;35m',
    'cyan': '\033[0;36m',
    'gray': '\033[0;37m'
}


class Color(object):

    @staticmethod
    def cprint(color_name: str, text: str, next_line: bool = True):
        if next_line:
            text += '\n'
        if config.COLOR_OUT:
            try:
                subprocess.run(
                    "echo -e -n " + "\"" + colors[color_name] + text + colors["white"] + "\"")
            except Exception:
                raise ValueError("Color not found")
        else:
            stdout.write(text)
        stdout.flush()

    config.IS_WINDOWS = False
    print("platform: ", end='')
    if platform[:5] == "linux":
        cprint("green", "Linux, color on")
    else:
        config.COLOR_OUT = False
        if platform == "win32":
            config.IS_WINDOWS = True
            print("Windows", end='')
        else:
            print("Mac OS", end='')
        print(", color off")

    @staticmethod
    def clear():
        if config.IS_WINDOWS:
            system("cls")
        else:
            system("clear")

    @staticmethod
    def logo():
        Color.cprint("cyan", LOGO)
