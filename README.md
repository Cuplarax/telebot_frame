
#### *Простой каркас для создания телеграмм бота.*

#### *A simple frame for creating telegram bots.*

    (english description below)

Чтобы воспользоваться этим кодом, требуется в терминале bash прописать следующую команду:
>git clone https://codeberg.org/Cuplarax/telebot_frame.git && cd telebot_frame

Чтобы запустить базового бота пропишите:
>python main.py

Чтобы изменять команды бота вы можете создавать и редактировать функции в файле *code/params/bot_commands.py*

При запуске бота сразу же будет запущен локальный сервер и сразу же откроется файл *code/connect/client.py*
Он представляет из себя локальный клиент, который будет пытаться подключаться к локальному серверу, что позволит выполнять команды.

команды - пояснение:
* `params` - вывод порта и размера передаваемых пакетов
* `stop` - останавливает бота и закрывает сервер с клиентом (такой механизм сделан из-за невозможности ввода в основном окне)
* `hello` - сервер отвечает фразой "hi!" (просто проверка соединения)

Любые другие команды будут расценены как неизвестные и в ответом на них будет фраза "unknown command"

---
english description

To use this code, you need to write the following command in the bash terminal:
>git clone https://codeberg.org/Cuplarax/telebot_frame.git && cd telebot_frame

To launch the basic bot, write:
>python main.py

To change the bot's commands, you can create and edit functions in the file *code/params/bot_commands.py*

When the bot starts, the local server will be started immediately and the file will open *code/connect/client.py*
It is a local client that will try to connect to the local server, which will allow you to execute commands.

commands - explanation:
* `params` - output of the port and size of transmitted packets
* `stop` - stops the bot and closes the server with the client (this mechanism is made due to the inability to enter in the main window)
* `hello` - the server responds with the phrase "hi!" (just checking the connection)

Any other commands will be regarded as unknown and the response to them will be the phrase "unknown command"

```mermaid
flowchart LR
Z{main.py} --launch--> B[bot]
Z --launch--> C[local server]
C --launch--> D[local client]
C -.control.-> B
C <-.connect.-> D

```
