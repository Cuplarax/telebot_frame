try:
    import telebot
except ImportError:
    raise ImportError("pls install the telebot lib\npip install pyTelegramBotAPI")
import telebot
import sys
import threading
from code.params import config
from code.filer import Filer
from code.color import Color
from code.connect.server import Server


def get_args():
    args = sys.argv  # getting arguments
    args.pop(0)
    for argument in args:
        match argument.lower():
            case 'c':
                config.COLOR_OUT = True
                Color.cprint('purple', 'forced color on')
            case 'r':
                config.TOKEN_FL = True
                Color.cprint('red', 'forced token load')
            case 's':
                config.CONNECT_FL = True
                Color.cprint('red', 'forced connect parameters load')
            case _:
                Color.cprint('orange', f'unknown argument \""{argument}\"" is ignored')


def prep():
    get_args()
    Color.logo()
    config.PORT = Filer.create_get('port', 'enter a port of socket connection', True, config.CONNECT_FL)
    config.PKG_SIZE = Filer.create_get('pkg_size', 'enter a package size of socket connection', True, config.CONNECT_FL)
    config.TOKEN = Filer.create_get('token', 'enter a bot token', False, config.TOKEN_FL)
    config.BOT = telebot.TeleBot(config.TOKEN)
    from code.params import bot_commands  # load the bot commands
    thread = threading.Thread(target=Server.start_server)
    thread.start()


def enter_point():
    prep()
    Color.cprint('green', 'bot is running...')
    config.BOT.polling()
    Color.cprint('red', 'bot was stopped')


if __name__ == '__main__':
    enter_point()
else:
    print(__name__)
    raise RuntimeError("pls launch the program from the file main.py")
